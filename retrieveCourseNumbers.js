$(document).ready(function(){
	  var text = $("#department :selected").val();
	  $("#load").append("<p>" + $("#department :selected").text() + " Loading</p>");
    $.get("result", {department: text}, function(data,status){
        var start = 0;
        var end = 0;
        if (data.length >= 7) {
      	end = 7;
          while (data.length >= end) {
      	  $("#courses").append(new Option(
      			  data.substring(start, end),
      		  data.substring(start, end)));
      	  	start+=7;
      	  	end+=7;
          }
        } else {
        	  $("#courses").append("<option>zero courses</option>");
        }
        $("p").remove();
      });
  $("#department").change(function() {
	  var text = $("#department :selected").val();
	  $("#load").append("<p>" + $("#department :selected").text() + " Loading</p>");
    $.get("result", {department: text}, function(data,status){
      $("#courses option").remove();
      var start = 0;
      var end = 0;
      if (data.length >= 7) {
    	end = 7;
        while (data.length >= end) {
    	  $("#courses").append(new Option(
    			  data.substring(start, end),
    		  data.substring(start, end)));
    	  start+=7;
    	  end+=7;
        }
      } else {
    	  $("#courses").append("<option>zero courses</option>");
      }
      $("p").remove();
    });
  });
});