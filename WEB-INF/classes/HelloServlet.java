import java.io.*;
import java.util.NavigableMap;

import javax.servlet.*;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet
{
	
	/**
	 * Retrieves the selection call from the department from index.html and displays
	 * the available courses.
	 * @param request, response
	 * @throws ServletException, IOException
	 */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

    	// file path of the package
    	String homeDirectory = req.getServletContext().getRealPath("/");
    	
    	// retrieves the selected department and checks if it is in the database
        String department = req.getParameter("department");
        if (!ConnectToDatabase.tableExist(department, homeDirectory)) {
        	// parses the RMP website of the selected department
        	// and store the information in the database
        	ConnectToDatabase.initDatabase(department, homeDirectory);
        }
        // retrieves all of the available courses from the selected department
        String courses = ConnectToDatabase.extractCourses(department, homeDirectory);

        // sends the courses to index.html
        resp.getWriter().write(courses);
    }

	/**
	 * Checks for all of the professors whom taught a selected course
	 * and displays both their name and rating in ascending order
	 * @param request, response
	 * @throws ServletException, IOException
	 */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
    	
    	// file path of the package
    	String homeDirectory = req.getServletContext().getRealPath("/");
    	
    	String department = req.getParameter("department");
    	String courseNumber = req.getParameter("courseNumber");
    	
    	// Retrieves a map of professors who taught the entered course
    	NavigableMap<Double, String> map = ConnectToDatabase.extractData(
    			courseNumber, department, homeDirectory);
    	
    	// sends the map of both the professors and their rating to the response
    	req.setAttribute("courseNumber", courseNumber);
        req.setAttribute("professors", map);
        
        // redirects the page to results.jsp
        req.getRequestDispatcher("WEB-INF/jsp/results.jsp").forward(req, resp);
    }
}