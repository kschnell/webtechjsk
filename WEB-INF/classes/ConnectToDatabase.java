import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ConnectToDatabase {
	
	/**
	 * Creates a table for the selected department and all of the courses being
	 * taught in it if one is not already created.
	 * @param department
	 * @param homeDirectory
	 */
	static void initDatabase(String department, String homeDirectory) {
		try {
			// Creates a connection to the database
			Connection connection = createConnection(homeDirectory);
	
			// check to see if table already exists
	    	String departmentName = department.replace("+", "_");
	    	departmentName = departmentName.replace("%26", "").replaceAll("'", "");
			boolean newTable = true;
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet resultSet = dbm.getTables(null, null, departmentName.toLowerCase().concat("_professors"), null);
			if (resultSet.next()) {
				newTable = false;
			}
			resultSet.close();

			if (newTable == true) {
				
				// Retrieve the data from My Rate Professor and parse it to a JSON file
		    	GrabData.getData(department, homeDirectory);
		    	
		    	// Retrieve and parse the JSON file
		    	JSONParser parser = new JSONParser();
		    	JSONObject json = (JSONObject) parser.parse(new FileReader(homeDirectory + "//data.json"));
		    	
				//	create courses table
				Statement st = connection.createStatement();
				String sql =
						"CREATE " +
						"TABLE " +
						departmentName + "_courses " +
						"(course_number VARCHAR(20), id INT)";
				st.execute(sql);
				
				HashMap<String, Integer> map = (HashMap<String, Integer>) json.get("COURSES");
				
				//	insert courses and their id to the courses table
				for (String key : map.keySet()) {
					sql =
						"INSERT INTO " + departmentName + "_courses " +
						"VALUES('" + key + "', " +
								map.get(key) + ")";
					st.execute(sql);
				}
				
				// Create the professors table
				sql =
					"CREATE " +
					"TABLE " +
					departmentName + "_professors " +
					"(name VARCHAR(50), rating DECIMAL(2,1), coursesID VARCHAR(300))";
				st.execute(sql);
				
				// Insert the name, rating, and id of courses for each professor to the professors table
				JSONArray array = (JSONArray) json.get("PROFESSORS");
				for (int count = 0; count < array.size(); count++) {
					JSONObject index = (JSONObject) array.get(count);
					sql =
						"INSERT INTO " + departmentName + "_professors " +
						"VALUES('" + index.get("NAME").toString().replaceAll("'", "\'") + "', " +
						index.get("RATING") + ", " +
						"'" + index.get("COURSEID").toString() + "')";
					st.execute(sql);
				}
				
				st.close();
			}
			connection.close();
		} catch (ClassNotFoundException | SQLException | IOException | ParseException e) {
	
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a connection to a PostgresSQL database
	 * @param homeDirectory
	 * @return the current database connection
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws FileNotFoundException
	 */
	private static Connection createConnection(String homeDirectory)
			throws ClassNotFoundException, SQLException, FileNotFoundException {
		// Brings the database driver into session
		Class.forName("org.postgresql.Driver");
		
		// Retrieves the database name, username, and password from the text file
		Scanner inputStream = new Scanner(new FileInputStream(homeDirectory + "//databaseCredentials.txt"));
		
		//	port:5432	database:RMP	username:postgres	password:test
		String database = inputStream.nextLine();
		String username = inputStream.nextLine();
		String password = inputStream.nextLine();
		inputStream.close();
		
		// open the connection
		return DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/" + database,
				username, password);
	}
	
	/**
	 * Checks to see if the selected department exists in the database
	 * @param department
	 * @param homeDirectory
	 * @return boolean value if the selected table exists or not
	 */
	public static boolean tableExist(String department, String homeDirectory) {
		boolean tableExist = false;
		try {
			// open the connection
			Connection connection = createConnection(homeDirectory);
	
			// check to see if table already exists
	    	String departmentName = department.replace("+", "_");
	    	departmentName = departmentName.replace("%26", "");
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet resultSet = dbm.getTables(null, null, departmentName.toLowerCase().concat("_professors"), null);
			if (resultSet.next()) {
				tableExist = true;
			}
			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException | FileNotFoundException e) {
	
			e.printStackTrace();
		}
		return tableExist;
	}
	
	/**
	 * Extracts all of the courses of a selected department
	 * @param department
	 * @param homeDirectory
	 * @return all of the courses
	 */
	public static String extractCourses(String department, String homeDirectory) {
		String courses = new String();
		try {
			// opens the connection
			Connection connection = createConnection(homeDirectory);
	
			// check to see if table already exists
	    	String departmentName = department.replace("+", "_");
	    	departmentName = departmentName.replace("%26", "").replaceAll("'", "");
			boolean newTable = true;
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet resultSet = dbm.getTables(null, null, departmentName.toLowerCase().concat("_professors"), null);
			if (resultSet.next()) {
				newTable = false;
			}
			resultSet.close();

			if (newTable == false) {
				// retrieves all of the course numbers in ascending order
				Statement st = connection.createStatement();
				String query = "SELECT course_number FROM " + departmentName + "_courses"
						+ " ORDER BY course_number ASC";;
				resultSet = st.executeQuery(query);

				// stores the result in a string
				while (resultSet.next()) {
					courses = courses.concat(resultSet.getString("COURSE_NUMBER"));
				}

			resultSet.close();
			st.close();
			}
			connection.close();
		} catch (ClassNotFoundException | SQLException | FileNotFoundException e) {
	
			e.printStackTrace();
		}
		return courses;
	}
	
	/**
	 * Extracts all of both the professors and their ratings for the selected
	 * course under the selected department
	 * @param courseNumber
	 * @param department
	 * @param homeDirectory
	 * @return NavigableMap
	 */
	public static NavigableMap<Double, String> extractData(String courseNumber, String department, String homeDirectory) {
		TreeMap<Double, String> map = new TreeMap<Double, String>();
		try {
			// opens the connection
			Connection connection = createConnection(homeDirectory);
	
			// check to see if table already exists
	    	String departmentName = department.replace("+", "_");
	    	departmentName = departmentName.replace("%26", "").replaceAll("'", "");
			boolean newTable = true;
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet resultSet = dbm.getTables(null, null, departmentName.toLowerCase().concat("_professors"), null);
			if (resultSet.next()) {
				newTable = false;
			}
			resultSet.close();

			if (newTable == false) {
				// retrieve the id for the corresponding course
				Statement st = connection.createStatement();
				String query = "SELECT id FROM " + departmentName + "_courses WHERE course_number='"
						+ courseNumber + "'";
				resultSet = st.executeQuery(query);
				
				// check to see if the query delivered any results
				if (resultSet.next()) {
					int id = resultSet.getInt("id");
						
					// retrieve each professor that teach the following course
					query = "SELECT name, rating FROM " + departmentName + "_professors "
							+ "WHERE coursesID like '%," + id + ",%' OR "
							+ "coursesID='[" + id + "]' OR "
							+ "coursesID like '[" + id + ",%' OR "
							+ "coursesID like '%," + id + "]' ";
					resultSet = st.executeQuery(query);
					
					// Store the results into a map
					while (resultSet.next()) {
						map.put(resultSet.getDouble("RATING"), resultSet.getString("NAME"));
					}
				}

			resultSet.close();
			st.close();
			}
			connection.close();
		} catch (ClassNotFoundException | SQLException | FileNotFoundException e) {
	
			e.printStackTrace();
		}
		return map.descendingMap();
	}
}