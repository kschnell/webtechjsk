import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GrabData {
	/**
	 * Parse the Rate My Professor website under the selected department
	 * and store all of its professors, their ratings, and the courses
	 * they are teaching in a JSON file
	 * @param department
	 * @param homeDirectory
	 */
	public static void getData(String department, String homeDirectory) {
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		Set<String> uniqueCourses = new TreeSet<String>();
		Vector<Integer> prime = new Vector<Integer>();
		JSONArray array = new JSONArray();
		
		try {
			
			// Creates the url for the Rate My Professor website
			int count = 1;
			String urlTeacher = "http://www.ratemyprofessors.com/ShowRatings.jsp?tid=";
			String url = "http://www.ratemyprofessors.com/SelectTeacher.jsp?the_dept="
						+ department + "&orderby=TLName&sid=1268";
			String urlPage = "&pageNo=";
			String urlTest = url.concat(urlPage + count);
			
			// loops through each page for a department
			while (true) {
				
				// Connects and retrieves the document from the main page including the department
				Document doc = Jsoup.connect(urlTest).get();
				Elements professors = doc.getElementsByClass("profName");
				Elements id = professors.select("a[href*=showRatings.jsp?tid]");
				
				// loops and connects to each professor
				for (Element path : id) {
					
					// retrieves the rating and name from the professor		
					doc = Jsoup.connect(urlTeacher + path.attr("href").substring(20)).get();
					
					// continues through the next loop iteration if the professor's rating does not exist
					Element rating = doc.getElementById("quality");
					if (rating == null)
						continue;
					
					// stores both the professors name and rating into a JSON object
					JSONObject professor = new JSONObject();
					professor.put("NAME", path.text());
					professor.put("RATING", new Double(Double.parseDouble(rating.select("strong").text())));
					Elements courses = doc.getElementsByClass("class");
					
					Set<Integer> courseID = new TreeSet<Integer>();
					// retrieves all of the courses
					for (Element course : courses) {
						// only allows courses under the format of 3 alpha characters and
						// 4 numeric characters to be considered to be stored in the database
						String courseName = course.select("p").text().toUpperCase();
						if (courseName.length() == 7 && courseName.substring(0, 3).matches("[A-Z]+")
								&& courseName.substring(3, 7).matches("[0-9]+")) {
							if (!uniqueCourses.contains(courseName)) {
								// creates an id for each course
								uniqueCourses.add(courseName);
								// stores the course in a map with an assigned id
								map.put(courseName, getPrime(prime));
							}
							// stores the course ID's in a map
							courseID.add(map.get(courseName));
						}
					}
					// stores all of the course ID's with the corresponding professor
					professor.put("COURSEID", courseID);
					array.add(professor);
				}
				// continues the iteration with the next page in the selected department
				urlTest = url.concat(urlPage + ++count);
			}
		} catch (IOException e) {
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		JSONObject json = new JSONObject();

		// places the map of the courses and its ids along with data from all of the
		// professors into a single json file
		json.put("PROFESSORS", array);
		json.put("COURSES", map);
		
		try {
			FileWriter file = new FileWriter(homeDirectory + "//data.json");
			file.write(json.toJSONString());
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Creates a prime number
	 * @param prime
	 * @return an unique prime number
	 */
	public static Integer getPrime(Vector<Integer> prime) {
		if (prime.size() == 0) {
			prime.add(2);
			return 2;
		}
		int num;
		num = prime.get(prime.size() - 1);
		num++;
		int count = 0;
		while (count < prime.size()) {
			if (num % prime.get(count) == 0) {
				num++;
				count = 0;
				continue;
			}
			count++;
		}
		prime.add(num);
		return num;
	}
}