<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>FAU Professors!</title>
    <link type="text/css" rel="stylesheet" href="results.css">
  </head>
  <body id = "gradient">
  
  	<h1>FAU <caption><b><c:out value="${courseNumber}" /></b></caption> Professors!</h1>
  	<%-- prints the course number to the screen --%>
	<table border="3" id="redTable" style="align:center" ">
		
    	<c:choose>
    		<%-- checks if the query resulted with any data or not --%>
    		<c:when test="${empty professors}">
    			<tr>
    				<td>No Results</td>
    			</tr>
    			
    		</c:when>
    		<c:otherwise>
    			<%-- prints each professor to the screen --%>
    			<tr style = " bgcolor:red; color:white" >
    				<th>Professor</th>
    				<th>Rating</th>
    			</tr>
    			<c:forEach  var="professor" items="${professors}">
					<tr style ="bgcolor:blue">
						<td><b>${professor.value}</b></td>
						<td><b>${professor.key}</b></td>
						
					</tr>
					
				</c:forEach>
    		</c:otherwise>
    	</c:choose>
    </table>
    <br />
    <%-- link to return to the index.html page --%>
    <a href="index.html" text-align="center">Return to try again! (CLICK ME)</a>
  </body>
</html>