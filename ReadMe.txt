This program was design to display all of the professors for a selected course in a department in ascending order to their rating.
The data used for this website is derived from the Rate My Professor website.

index.html
For the first time a specific department is selected, the applicaton will execute and parse the Rate My Professor website under the FAU Boca Raton
campus and extract all of the professors, their ratings, and courses under the selected department. This data is then stored into
a JSON file in which it is retrieved and stored in a PostgresSQL database. Finally, it appends all of the course numbers in
ascending order onto the index.html page as a selectable option.

When the "Check for Professor Ratings" button is clicked, the program then searches the selected department for all of the
professors who taught the selected course. This data is sent to the results.jsp page in which this data is displayed in
ascending order of the professor's ratings with their corresponding names.

This is how the WebTechJSK.war file can be deployed.

1.	Download Eclipse IDE for JavaEE developers. Import this file under web/war. You may have to install Tomcat 7 plugin.

By default, the Tomcat servlet runs on the port 8080.

Download PostGreSql as the database. Manually edit the file databaseCredentials.txt and input your database name, username, and password.

Run the Tomcat server in eclipse against the WebTechJSK application and enter the url into the browser, http://localhost:8080/WebTechJSK.
This will enable you to access the program.

2.	The war file can also be deployed by downloading Tomcat version 7 from the url http://tomcat.apache.org/download-70.cgi.
download the file and unzip it. Create two system variables; JAVA_HOME and JRE_HOME. The Java jdk 7 is also necessary.
Examples:
JAVA_HOME: java\jdk1.7.0\bin
JRE_HOME:  java\jre7
Place the WebTechJSK.war file or the WebTechJSK directory in the webapps directory in Tomcat. Run the startup.bat file in command prompt from the bin directory in Tomcat.
The url is http://localhost:8080/WebTechJSK

Source code:	https://bitbucket.org/kschnell/webtechjsk
Twitter:		https://twitter.com/WebTechJSK
Blog:			http://webtechjsk.blogspot.com/